import * as React from 'react';
import {
  Platform, StatusBar, StyleSheet, View,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-redux';

import Store from './redux/store';
import HomeScreen from './screens/HomeScreen';
import ImageCard from './screens/ImageCard';

// import BottomTabNavigator from './navigation/BottomTabNavigator';
// import useLinking from './navigation/useLinking';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});


const Stack = createStackNavigator();

export default function App() {
  // const [isLoadingComplete, setLoadingComplete] = React.useState(false);
  const [initialNavigationState] = React.useState();
  const containerRef = React.useRef();
  // const { getInitialState } = useLinking(containerRef);

  // Load any resources or data that we need prior to rendering the app
  // React.useEffect(() => {
  //   async function loadResourcesAndDataAsync() {
  //     try {
  //       SplashScreen.preventAutoHide();

  //       // Load our initial navigation state
  //       setInitialNavigationState(await getInitialState());

  //       // Load fonts
  //       await Font.loadAsync({
  //         ...Ionicons.font,
  //         'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
  //       });
  //     } catch (e) {
  //       // We might want to provide this error information to an error reporting service
  //       console.warn(e);
  //     } finally {
  //       setLoadingComplete(true);
  //       SplashScreen.hide();
  //     }
  //   }

  //   loadResourcesAndDataAsync();
  // }, []);

  // if (!isLoadingComplete && !props.skipLoadingScreen) {
  //   return null;
  // }
  return (
    <Provider store={Store}>
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <NavigationContainer ref={containerRef} initialState={initialNavigationState}>
          <Stack.Navigator>
            <Stack.Screen name="Root" component={HomeScreen} />
            <Stack.Screen name="ImageCard" component={ImageCard} />
          </Stack.Navigator>
        </NavigationContainer>
      </View>
    </Provider>
  );
}
