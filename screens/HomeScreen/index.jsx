import React from 'react';
import {
  Text,
  View,
  TextInput,
  ScrollView,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { imageSearchRequest } from '../../redux/actions';

import SearchItem from './SearchItem';
import styles from './styles';

function HomeScreen(props) {
  const { navigation } = props;
  const dispatch = useDispatch();
  const data = useSelector((state) => state.imageSearch);

  console.log(props);
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Image search</Text>
      <View style={styles.inputWrapper}>
        <TextInput
          style={styles.input}
          placeholder="Search"
          onSubmitEditing={() => dispatch(imageSearchRequest())}
        />
      </View>
      <ScrollView>
        <SearchItem data={data} navigation={navigation} />
      </ScrollView>
    </View>
  );
}

export default HomeScreen;
