import React from 'react';
import {
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';

import styles from './styles';


function SearchItem(props) {
  const { data: { data, loading }, navigation } = props;

  return (
    <View style={styles.wrapper}>
      {data.items && !loading && (
        data.items.map((item) => {
          return (
            <TouchableOpacity onPress={() => navigation.navigate('ImageCard', item)} key={item.title}>
              <ImageBackground source={{ uri: item.link }} style={styles.image}>
                <View style={styles.textWrapper}>
                  <Text style={styles.text} numberOfLines={1}>{item.title}</Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          );
        })
      )}
    </View>
  );
}

export default SearchItem;
