import { StyleSheet, Dimensions } from 'react-native';

import { width } from '../../../constants/Layout';

const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
    width: '100%',
  },
  image: {
    width: width * 0.35,
    height: 100,
    marginTop: 20,
  },
  textWrapper: {
    marginTop: 'auto',
    backgroundColor: '#BEB9B8',
    borderWidth: 1,
    opacity: 0.8,
    padding: 5,
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#000',
  },
});

export default styles;
