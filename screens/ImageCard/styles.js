import { StyleSheet } from 'react-native';

import { height } from '../../constants/Layout';


const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    height: '100%',
    backgroundColor: '#000',
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: height * 0.3,
  },
  imageWrapper: {
    height: '100%',
    flex: 1,
    justifyContent: 'center',
  },
  textWrapper: {
    position: 'absolute',
    bottom: 0,
    padding: 5,
    width: '100%',
    backgroundColor: '#BEB9B8', // TODO вынести textWrapper и text в глобал
    borderWidth: 1,
    opacity: 0.8,
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#000',
  },
});

export default styles;
