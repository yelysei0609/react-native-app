import React from 'react';
import {
  Text,
  View,
  ImageBackground,
  ScrollView,
} from 'react-native';

import styles from './styles';

function ImageCard(props) {
  const { route: { params } } = props;
  console.log(props, 'items card');
  console.log(params, 'data');

  return (
    <View style={styles.wrapper}>
      <ScrollView contentContainerStyle={styles.imageWrapper} minimumZoomScale={1} maximumZoomScale={5}>
        <ImageBackground source={{ uri: params.link }} style={styles.image} />
      </ScrollView>
      <View style={styles.textWrapper}>
        <Text style={styles.text}>{params.title}</Text>
      </View>
    </View>
  );
}

export default ImageCard;
