import mock from '../screens/HomeScreen/SearchItem/mock.json';

class Api {
  getImages() {
    return Promise.resolve(mock);
  }
}

export default new Api();
