export const IMAGE_SEARCH_REQUEST = 'image.search.request';
export const IMAGE_SEARCH_SUCCESS = 'image.search.success';
export const IMAGE_SEARCH_FAILURE = 'image.search.failure';

export const imageSearchRequest = () => ({
  type: IMAGE_SEARCH_REQUEST,
});

export const imageSearchSuccess = (payload) => ({
  type: IMAGE_SEARCH_SUCCESS,
  payload,
});

export const imageSearchFailure = (payload) => ({
  type: IMAGE_SEARCH_FAILURE,
  payload,
});
