import {
  createStore,
  applyMiddleware,
  combineReducers,
} from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';

import imageSearch from './reducers';

const sagaMiddleware = createSagaMiddleware();

const reducers = { imageSearch };
const rootReducer = combineReducers(reducers);
const Store = createStore(rootReducer, applyMiddleware(logger, sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default Store;
