import { call, put, takeLatest } from 'redux-saga/effects';

import {
  IMAGE_SEARCH_REQUEST,
  imageSearchSuccess,
  imageSearchFailure,
} from '../actions';

import api from '../../api';

function* imageSearchRequestSaga() {
  try {
    const data = yield call(api.getImages);
    yield put(imageSearchSuccess(data));
  } catch (err) {
    yield put(imageSearchFailure(err));
  }
}

function* imageSearchSaga() {
  yield takeLatest(IMAGE_SEARCH_REQUEST, imageSearchRequestSaga);
}

export default imageSearchSaga;
