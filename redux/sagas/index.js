import { fork, all } from 'redux-saga/effects';

import searchImageSaga from './imageSearch.saga';

function* rootSaga() {
  yield all([
    fork(searchImageSaga),
  ]);
}

export default rootSaga;
