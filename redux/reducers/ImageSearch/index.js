import {
  IMAGE_SEARCH_REQUEST,
  IMAGE_SEARCH_SUCCESS,
  IMAGE_SEARCH_FAILURE,
} from '../../actions';

const initState = {
  loading: false,
  data: [],
  error: null,
};

const imageSearch = (state = initState, action) => {
  const { payload, type } = action;

  switch (type) {
    case IMAGE_SEARCH_REQUEST:
      return { ...state, loading: true };

    case IMAGE_SEARCH_SUCCESS:
      return { ...state, data: payload, loading: false };

    case IMAGE_SEARCH_FAILURE:
      return { ...state, error: payload, loading: false };

    default: return state;
  }
};

export default imageSearch;
